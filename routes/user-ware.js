
var db = require('../config/dbschema');

exports.addUser = function(user, cb) {
  adm = (user.admin === "true");

  var newUser = new db.userModel({username: user.username,
    fullname: user.fullname,
    email: user.email,
    password: user.password,
    admin: adm});

  newUser.save(function(err) {
    if(err) {
      console.log('Error: ', err);
      return cb(err);
    } else {
      console.log('Saved user: ' + user.username);
      return cb(null);
    }
  });

}
