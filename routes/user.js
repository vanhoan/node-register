var passport = require('passport');
var cleanString = require('../helpers/clean-string');
var userWare = require('./user-ware');

exports.account = function(req, res) {
  res.render('account', { user: req.user });
};

exports.getLogin = function(req, res) {
  res.render('login', { user: req.user, message: 'req.session.messages'});
};

exports.admin = function(req, res) {
  res.send('you do access granted admin!');
};

// POST /login
//   This is an alternative implementation that uses a custom callback to
//   acheive the same functionality.
exports.postLogin = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err) }
    if (!user) {
      req.session.messages =  [info.message];
      return res.redirect('/login')
    }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/');
    });
  })(req, res, next);
};

exports.logout = function(req, res) {
  req.logout();
  res.redirect('/');
};

exports.getRegister = function(req, res) {
  res.render('signup', { user: req.user, message: 'req.session.messages'});
}

exports.postRegister = function(req, res, next) {
  var username = cleanString(req.body.username);
  var fullname = cleanString(req.body.fullname);
  var email = cleanString(req.body.email);
  var password = cleanString(req.body.password);

  if (!(username && fullname && email && password)) {
    return invalid();
  }
  // user friendly
  email = email.toLowerCase();
  username = username.toLowerCase();

  newRegister = {username: req.body.username,
    fullname: req.body.fullname,
    email: req.body.email,
    password: req.body.password};

  userWare.addUser(newRegister, function(err) {
    if (err) {
      if (err instanceof mongoose.Error.ValidationError) { return invalid(); } 
      return next(err);
    }
    return res.redirect('/login');
  });

  function invalid () {
    return res.render('signup', { invalid: true });
  }
}
